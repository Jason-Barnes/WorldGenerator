var noiseScale = 0.07;
var overLayHud;

// Editable elevation color limits
let oceanLevelLimit = 0.42;
let grasslandLevelLimit = 0.60;
let dirtLevelLimit = 0.67;
let rockLevelLimit = 0.73;

let elevationLimitUpdated = true; // slider change flag
let autoUpdate = true; // auto update terrain flag

// p5 one-time setup function
function setup() {
  createCanvas(displayWidth, displayHeight);  
  noStroke();
  noSmooth();
  blendMode(BLEND); // for background of HUD 

  overLayHud = new HUD();
  drawTerrain();
}

// p5 looping draw function
function draw() {
  voronoiDraw(0, 0);
  overLayHud.drawHud();
}

// draw terrain with Voronoi cells for natural shapes
function drawTerrain() {
  if (!elevationLimitUpdated) return;

  voronoiSiteFlag(false);
  voronoiCellStrokeWeight(0);
  voronoiRndMinDist(1);

  for (let y = 0; y < displayHeight/30; y++) {
    for (let x = 0; x < displayWidth/30; x++) {
      // Generate noise
      let noiseVal = noise(x*noiseScale, y*noiseScale);
      let noiseValNormalized = noiseVal * 100;

      // set color of cell dependent on noise value (i.e. "elevation")
      colorVoronoiCells(x, y, noiseVal, noiseValNormalized);
    }
  }
  // draw all cells
  voronoi(displayWidth, displayHeight);
  elevationLimitUpdated = false;
}

// set color of cell dependent on noise value (i.e. "elevation")
function colorVoronoiCells(x, y, noiseVal, noiseValNormalized) {
  // Set landscape colors (on gradients)
  let ocean = color(noiseValNormalized, noiseValNormalized*2, 255);
  let grassland = color(noiseValNormalized, 200-noiseVal*150, noiseValNormalized);
  let dirt = color(150 - noiseValNormalized, 100-noiseVal*70, 0);
  let rock = color(noiseValNormalized, noiseValNormalized, noiseValNormalized);
  const snow = color(255);

  let terrainColor;
  if (noiseVal < oceanLevelLimit) terrainColor = ocean;
  else if (noiseVal < grasslandLevelLimit) terrainColor = grassland;
  else if (noiseVal < dirtLevelLimit) terrainColor = dirt;
  else if (noiseVal < rockLevelLimit) terrainColor = rock;
  else terrainColor = snow;

  // set cell
  voronoiSite(x*noiseValNormalized, y*noiseValNormalized, terrainColor);
}

// Heads-Up display for editing values of elevation color limits
class HUD {
  constructor(colorHud = color(0, 0, 0, 175)) {
    this.colorHud = colorHud;

    this.oceanSlider = createSlider(0, 1, oceanLevelLimit, 0.01).position(35, 100);
    this.grassSlider = createSlider(0, 1, grasslandLevelLimit, 0.01).position(35, 175);
    this.dirtSlider = createSlider(0, 1, dirtLevelLimit, 0.01).position(35, 250);
    this.rockSlider = createSlider(0, 1, rockLevelLimit, 0.01).position(35, 325);

    this.autoUpdateCheckBox = createCheckbox("", true)
                              .changed(this.onAutoUpdateCheck)
                              .position(200, 360);

    this.regenerateTerrainButton = createButton("REGENERATE")
                                   .mousePressed(this.regenerateTerrain)
                                   .position(100, 400);
  };

  drawHud() {
    noStroke();
    fill(this.colorHud);
    rect(25, 25, 250, 425); // background

    // Title header
    fill(255)
      .strokeWeight(10)
      .textSize(24);
    text("Terrain Generator", 50, 50);

    // Slider Labels
    textSize(16);
    text("Ocean Elevation", 50, this.oceanSlider.y-5);
    text("Grassland Elevation", 50, this.grassSlider.y-5);
    text("Earth Elevation", 50, this.dirtSlider.y-5);
    text("Rock Elevation", 50, this.rockSlider.y-5);

    // Checkbox Label
    text("Auto-Update?", 75, this.autoUpdateCheckBox.y + 15);

    // Slider Values
    textSize(24);
    text(this.oceanSlider.value(), 50 + this.oceanSlider.width, this.oceanSlider.y + this.oceanSlider.height);
    text(this.grassSlider.value(), 50 + this.grassSlider.width, this.grassSlider.y + this.grassSlider.height);
    text(this.dirtSlider.value(), 50 + this.dirtSlider.width, this.dirtSlider.y + this.dirtSlider.height);
    text(this.rockSlider.value(), 50 + this.rockSlider.width, this.rockSlider.y + this.rockSlider.height);
  }

  // Set elevation limits to respective slider values
  updateLimitValues() {
    if (this.oceanSlider.value() != oceanLevelLimit ||
        this.grassSlider.value() != grasslandLevelLimit ||
        this.dirtSlider.value() != dirtLevelLimit ||
        this.rockSlider.value() != rockLevelLimit) {
      elevationLimitUpdated = true;
      oceanLevelLimit = this.oceanSlider.value();
      grasslandLevelLimit = this.grassSlider.value();
      dirtLevelLimit = this.dirtSlider.value();
      rockLevelLimit = this.rockSlider.value();      
    }
  }

  // update functions
  onAutoUpdateCheck = () => { autoUpdate = this.autoUpdateCheckBox.checked(); };
  regenerateTerrain = () => { if(!autoUpdate) {this.updateLimitValues(); drawTerrain();} };
}

// On release of sliders, auto-update
function mouseReleased() {
  if (!autoUpdate) return;
  overLayHud.updateLimitValues();
  drawTerrain();
}